//
//  Slots888ApiClient.h
//  CasinoAgregator
//
//  Created by Pavel Wasilenko on 03.05.17.
//  Copyright © 2017 Alexey. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Slots888ApiClient : NSObject

- (NSString *)sendRequest;

@end
